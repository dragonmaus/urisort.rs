use serde::{Deserialize, Serialize};
use std::{io, str};

#[derive(Deserialize, Eq, Ord, PartialEq, PartialOrd, Serialize)]
pub struct Path {
    inner: Vec<u8>,
}

impl From<Vec<u8>> for Path {
    fn from(inner: Vec<u8>) -> Self {
        Self { inner }
    }
}

impl Into<Vec<u8>> for Path {
    fn into(self) -> Vec<u8> {
        self.inner
    }
}

pub fn sort_paths<I: Iterator<Item = Vec<u8>>>(iter: I) -> io::Result<I> {
    Ok(iter)
}

use getopt::prelude::*;
use my::{program_main, util};
use std::{
    error::Error,
    io::{self, BufRead},
    str,
};
use urisort::pathsort::sort_paths;

program_main!("pathsort");

fn usage_line() -> String {
    format!("Usage: {} [-0h]", util::program_name("pathsort"))
}

fn print_usage() -> Result<i32, Box<dyn Error>> {
    println!("{}", usage_line());
    println!("  -0   lines are terminated by NUL, not newline");
    println!("  -h   display this help");
    Ok(0)
}

fn program() -> Result<i32, Box<dyn Error>> {
    let mut opts = Parser::new(&util::program_args(), "0h");

    let mut eol = 10;
    loop {
        match opts.next().transpose()? {
            None => break,
            Some(opt) => match opt {
                Opt('0', None) => eol = 0,
                Opt('h', None) => return print_usage(),
                _ => unreachable!(),
            },
        }
    }

    for line in sort_paths(io::stdin().lock().split(eol))? {
        eprintln!("{}", str::from_utf8(&line?).map_err(|e| io::Error::new(io::ErrorKind::InvalidInput, e))?);
    }

    //for line in sort_paths(io::stdin().lock().split(eol))? {
    //    print!(
    //        "{}{}",
    //        str::from_utf8(&line?).map_err(|e| io::Error::new(io::ErrorKind::InvalidInput, e))?,
    //        eol
    //    );
    //}

    Ok(0)
}
